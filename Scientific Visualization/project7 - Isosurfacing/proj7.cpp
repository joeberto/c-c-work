/*=========================================================================

  Program:   Visualization Toolkit
  Module:    SpecularSpheres.cxx

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
//
// This examples demonstrates the effect of specular lighting.
//
#include "tricase.cxx"
#include "TriangleList.h"
#include "vtkSmartPointer.h"
#include "vtkSphereSource.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkInteractorStyle.h"
#include "vtkObjectFactory.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkProperty.h"
#include "vtkCamera.h"
#include "vtkLight.h"
#include "vtkOpenGLPolyDataMapper.h"
#include "vtkJPEGReader.h"
#include "vtkImageData.h"
#include <vtkPNGWriter.h>

#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkPolyDataReader.h>
#include <vtkPoints.h>
#include <vtkUnsignedCharArray.h>
#include <vtkFloatArray.h>
#include <vtkDoubleArray.h>
#include <vtkCellArray.h>
#include <vtkDataSetReader.h>
#include <vtkContourFilter.h>
#include <vtkRectilinearGrid.h>

#include <vtkCamera.h>
#include <vtkDataSetMapper.h>
#include <vtkRenderer.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkSmartPointer.h>


// ****************************************************************************
//  Function: GetNumberOfPoints
//
//  Arguments:
//     dims: an array of size 3 with the number of points in X, Y, and Z.
//           2D data sets would have Z=1
//
//  Returns:  the number of points in a rectilinear mesh
//
// ****************************************************************************

int GetNumberOfPoints(const int *dims)
{
    // 3D
    return dims[0]*dims[1]*dims[2];
    // 2D
    ///return dims[0]*dims[1];
}

// ****************************************************************************
//  Function: GetNumberOfCells
//
//  Arguments:
//
//      dims: an array of size 3 with the number of points in X, Y, and Z.
//            2D data sets would have Z=1
//
//  Returns:  the number of cells in a rectilinear mesh
//
// ****************************************************************************

int GetNumberOfCells(const int *dims)
{
    // 3D
    return (dims[0]-1)*(dims[1]-1)*(dims[2]-1);
    // 2D
    //return (dims[0]-1)*(dims[1]-1);
}


// ****************************************************************************
//  Function: GetPointIndex
//
//  Arguments:
//      idx:  the logical index of a point.
//              0 <= idx[0] < dims[0]
//              1 <= idx[1] < dims[1]
//              2 <= idx[2] < dims[2] (or always 0 if 2D)
//      dims: an array of size 3 with the number of points in X, Y, and Z.
//            2D data sets would have Z=1
//
//  Returns:  the point index
//
// ****************************************************************************

int GetPointIndex(const int *idx, const int *dims)
{
    // 3D
    return idx[2]*dims[0]*dims[1]+idx[1]*dims[0]+idx[0];
    // 2D
    //return idx[1]*dims[0]+idx[0];
}


// ****************************************************************************
//  Function: GetCellIndex
//
//  Arguments:
//      idx:  the logical index of a cell.
//              0 <= idx[0] < dims[0]-1
//              1 <= idx[1] < dims[1]-1 
//              2 <= idx[2] < dims[2]-1 (or always 0 if 2D)
//      dims: an array of size 3 with the number of points in X, Y, and Z.
//            2D data sets would have Z=1
//
//  Returns:  the cell index
//
// ****************************************************************************

int GetCellIndex(const int *idx, const int *dims)
{
    // 3D
    return idx[2]*(dims[0]-1)*(dims[1]-1)+idx[1]*(dims[0]-1)+idx[0];
    // 2D
    //return idx[1]*(dims[0]-1)+idx[0];
}

// ****************************************************************************
//  Function: GetLogicalPointIndex
//
//  Arguments:
//      idx (output):  the logical index of the point.
//              0 <= idx[0] < dims[0]
//              1 <= idx[1] < dims[1] 
//              2 <= idx[2] < dims[2] (or always 0 if 2D)
//      pointId:  a number between 0 and (GetNumberOfPoints(dims)-1).
//      dims: an array of size 3 with the number of points in X, Y, and Z.
//            2D data sets would have Z=1
//
//  Returns:  None (argument idx is output)
//
// ****************************************************************************

void GetLogicalPointIndex(int *idx, int pointId, const int *dims)
{
    // 3D
    idx[0] = pointId%dims[0];
    idx[1] = (pointId/dims[0])%dims[1];
    idx[2] = pointId/(dims[0]*dims[1]);

    // 2D
    //idx[0] = pointId%dims[0];
    //idx[1] = pointId/dims[0];
}


// ****************************************************************************
//  Function: GetLogicalCellIndex
//
//  Arguments:
//      idx (output):  the logical index of the cell index.
//              0 <= idx[0] < dims[0]-1
//              1 <= idx[1] < dims[1]-1 
//              2 <= idx[2] < dims[2]-1 (or always 0 if 2D)
//      cellId:  a number between 0 and (GetNumberOfCells(dims)-1).
//      dims: an array of size 3 with the number of points in X, Y, and Z.
//            2D data sets would have Z=1
//
//  Returns:  None (argument idx is output)
//
// ****************************************************************************

void GetLogicalCellIndex(int *idx, int cellId, const int *dims)
{
    // 3D
    idx[0] = cellId%(dims[0]-1);
    idx[1] = (cellId/(dims[0]-1))%(dims[1]-1);
    idx[2] = cellId/((dims[0]-1)*(dims[1]-1));

    // 2D
    //idx[0] = cellId%(dims[0]-1);
    //idx[1] = cellId/(dims[0]-1);
}



void
SetEdgePoints(int edges[12][2], int pts[8][3], const int* dims){

    //edge 0 - v0, v1
    edges[0][0] = GetPointIndex(pts[0], dims); edges[0][1] = GetPointIndex(pts[1],dims);
    //edge 1 - 
    edges[1][0] = GetPointIndex(pts[1], dims); edges[1][1] = GetPointIndex(pts[3],dims);
    //edge 2 - 
    edges[2][0] = GetPointIndex(pts[2], dims); edges[2][1] = GetPointIndex(pts[3],dims);
    //edge 3 - 
    edges[3][0] = GetPointIndex(pts[0], dims); edges[3][1] = GetPointIndex(pts[2],dims);

    //edge 4 - v4,  v5
    edges[4][0] = GetPointIndex(pts[4], dims); edges[4][1] = GetPointIndex(pts[5],dims);
    //edge 5 - 
    edges[5][0] = GetPointIndex(pts[5], dims); edges[5][1] = GetPointIndex(pts[7],dims);
    //edge 6 - 
    edges[6][0] = GetPointIndex(pts[6], dims); edges[6][1] = GetPointIndex(pts[7],dims);
    //edge 7 - 
    edges[7][0] = GetPointIndex(pts[4], dims); edges[7][1] = GetPointIndex(pts[6],dims);

    //edge 8 - 
    edges[8][0] = GetPointIndex(pts[0], dims); edges[8][1] = GetPointIndex(pts[4],dims);
    //edge 9 - 
    edges[9][0] = GetPointIndex(pts[1], dims); edges[9][1] = GetPointIndex(pts[5],dims);
    //edge 10 - 
    edges[10][0] = GetPointIndex(pts[2], dims); edges[10][1] = GetPointIndex(pts[6],dims);
    //edge 11 - 
    edges[11][0] = GetPointIndex(pts[3], dims); edges[11][1] = GetPointIndex(pts[7],dims);

}

void 
SetNumTriangles(int tris[256], int triCase[256][16]){
    //for each case, set how many triangles present in case
    int i;
    int j = 0;
    int count = 0;
    for (i=0; i < 256; i++){
        while(triCase[i][j] != -1){
            count++;
            j++;
        }
        tris[i] = count/3;
        count = 0;
        j = 0;
    }

}

void
GetEdgePoints(int pts[2],int edge){
    if(edge == 0){ pts[0] = 0; pts[1] = 1; return; }
    if (edge == 1){ pts[0] = 1; pts[1] = 3; return; }
    if (edge == 2){ pts[0] = 2; pts[1] = 3; return; }
    if (edge == 3){ pts[0] = 0; pts[1] = 2; return; }
    if(edge == 4) {pts[0] = 4; pts[1] = 5; return; }
    if (edge == 5){ pts[0] = 5; pts[1] = 7; return; }
    if (edge == 6){ pts[0] = 6; pts[1] = 7; return; }
    if (edge == 7){ pts[0] = 4; pts[1] = 6; return; }
    if(edge == 8){ pts[0] = 0; pts[1] = 4; return; }
    if (edge == 9){ pts[0] = 1; pts[1] = 5; return; }
    if (edge == 10){ pts[0] = 2; pts[1] = 6; return; }
    if (edge == 11){ pts[0] = 3; pts[1] = 7; return;}
    else{
        //printf("your iCase/edges are wrong. not beteen 0-3. Edge is: %i\n", edge );
        return;
    }
}


int main()
{

    int  i, j, k;

    vtkDataSetReader *rdr = vtkDataSetReader::New();
    rdr->SetFileName("proj6B.vtk");
    rdr->Update();

    int dims[3] = {0,0,0};
    vtkRectilinearGrid *rgrid = (vtkRectilinearGrid *) rdr->GetOutput();
    rgrid->GetDimensions(dims);

    float *X = (float *) rgrid->GetXCoordinates()->GetVoidPointer(0);
    float *Y = (float *) rgrid->GetYCoordinates()->GetVoidPointer(0);
    float *Z = (float *) rgrid->GetZCoordinates()->GetVoidPointer(0);
    float *F = (float *) rgrid->GetPointData()->GetScalars()->GetVoidPointer(0);

    TriangleList tl;
    //all needs to be 3D now
    int pts[8][3];
    int tris[256];
    int idx[3];
    int edges[12][2];
    int Tpts[2];
    float t;
    float pt1[2], pt2[2], pt3[2];
    int ntriangles;
    int iCase;
    int edge1, edge2, edge3; 
    SetNumTriangles(tris, triCase);

    //for every cell in the mesh
    for(i=0; i < GetNumberOfCells(dims); i++){

        GetLogicalCellIndex(idx, i, dims);

        pts[0][0] = idx[0];   pts[0][1] = idx[1];   pts[0][2] = idx[2];
        pts[1][0] = idx[0]+1; pts[1][1] = idx[1];   pts[1][2] = idx[2];
        pts[2][0] = idx[0];   pts[2][1] = idx[1]+1; pts[2][2] = idx[2];
        pts[3][0] = idx[0]+1; pts[3][1] = idx[1]+1; pts[3][2] = idx[2];
        pts[4][0] = idx[0];   pts[4][1] = idx[1];   pts[4][2] = idx[2]+1;
        pts[5][0] = idx[0]+1; pts[5][1] = idx[1];   pts[5][2] = idx[2]+1;
        pts[6][0] = idx[0];   pts[6][1] = idx[1]+1; pts[6][2] = idx[2]+1;
        pts[7][0] = idx[0]+1; pts[7][1] = idx[1]+1; pts[7][2] = idx[2]+1;

        iCase=0;
        //for each point in cube
        for(k=0; k<8; k++){
            //see if value is less than equal to isoval
            if (F[GetPointIndex(pts[k], dims)] > 3.2){
                iCase += pow(2,k); 
            }
        }

        SetEdgePoints(edges, pts, dims);
        ntriangles = tris[iCase]; 
        //for each triangle in the cell
        for (k=0; k < ntriangles; k++){

            edge1 = triCase[iCase][3*k];
            GetEdgePoints(Tpts,edge1);
            t = (3.2 - F[edges[edge1][0]])/(F[edges[edge1][1]] - F[edges[edge1][0]]);
            pt1[0] = X[pts[Tpts[0]][0]] + t*(X[pts[Tpts[1]][0]] - X[pts[Tpts[0]][0]]);
            pt1[1] = Y[pts[Tpts[0]][1]] + t*(Y[pts[Tpts[1]][1]] - Y[pts[Tpts[0]][1]]);
            pt1[2] = Z[pts[Tpts[0]][2]] + t*(Z[pts[Tpts[1]][2]] - Z[pts[Tpts[0]][2]]);

            edge2 = triCase[iCase][3*k+1];
            GetEdgePoints(Tpts,edge2);
            t = (3.2 - F[edges[edge2][0]])/(F[edges[edge2][1]] - F[edges[edge2][0]]);
            pt2[0] = X[pts[Tpts[0]][0]] + t*(X[pts[Tpts[1]][0]] - X[pts[Tpts[0]][0]]);
            pt2[1] = Y[pts[Tpts[0]][1]] + t*(Y[pts[Tpts[1]][1]] - Y[pts[Tpts[0]][1]]);
            pt2[2] = Z[pts[Tpts[0]][2]] + t*(Z[pts[Tpts[1]][2]] - Z[pts[Tpts[0]][2]]);

            edge3 = triCase[iCase][3*k+2];
            GetEdgePoints(Tpts,edge3);
            t = (3.2 - F[edges[edge3][0]])/(F[edges[edge3][1]] - F[edges[edge3][0]]);
            pt3[0] = X[pts[Tpts[0]][0]] + t*(X[pts[Tpts[1]][0]] - X[pts[Tpts[0]][0]]);
            pt3[1] = Y[pts[Tpts[0]][1]] + t*(Y[pts[Tpts[1]][1]] - Y[pts[Tpts[0]][1]]);
            pt3[2] = Z[pts[Tpts[0]][2]] + t*(Z[pts[Tpts[1]][2]] - Z[pts[Tpts[0]][2]]);

            tl.AddTriangle(pt1[0], pt1[1], pt1[2], pt2[0], pt2[1], pt2[2], pt3[0], pt3[1], pt3[2]);
        }
    }

    vtkPolyData *pd = tl.MakePolyData();

    //This can be useful for debugging
/*
    vtkDataSetWriter *writer = vtkDataSetWriter::New();
    writer->SetFileName("paths.vtk");
    writer->SetInputData(pd);
    writer->Write();
 */

    vtkSmartPointer<vtkDataSetMapper> win1Mapper =
      vtkSmartPointer<vtkDataSetMapper>::New();
    win1Mapper->SetInputData(pd);
    win1Mapper->SetScalarRange(0, 0.15);

    vtkSmartPointer<vtkActor> win1Actor =
      vtkSmartPointer<vtkActor>::New();
    win1Actor->SetMapper(win1Mapper);

    vtkSmartPointer<vtkRenderer> ren1 =
      vtkSmartPointer<vtkRenderer>::New();

    vtkSmartPointer<vtkRenderWindow> renWin =
      vtkSmartPointer<vtkRenderWindow>::New();
    renWin->AddRenderer(ren1);

    vtkSmartPointer<vtkRenderWindowInteractor> iren =
      vtkSmartPointer<vtkRenderWindowInteractor>::New();
    iren->SetRenderWindow(renWin);
    ren1->AddActor(win1Actor);
    ren1->SetBackground(0.0, 0.0, 0.0);
    renWin->SetSize(800, 800);

    ren1->GetActiveCamera()->SetFocalPoint(0,0,0);
    ren1->GetActiveCamera()->SetPosition(0,0,50);
    ren1->GetActiveCamera()->SetViewUp(0,1,0);
    ren1->GetActiveCamera()->SetClippingRange(20, 120);
    ren1->GetActiveCamera()->SetDistance(30);

    // This starts the event loop and invokes an initial render.
    //
    iren->Initialize();
    iren->Start();

    pd->Delete();
}
