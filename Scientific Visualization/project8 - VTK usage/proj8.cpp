#include "vtkSmartPointer.h"
#include "vtkSphereSource.h"
#include "vtkPolyDataMapper.h"
#include "vtkActor.h"
#include "vtkInteractorStyle.h"
#include "vtkObjectFactory.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkProperty.h"
#include "vtkCamera.h"
#include "vtkLight.h"
#include "vtkOpenGLPolyDataMapper.h"
#include "vtkJPEGReader.h"
#include "vtkImageData.h"

#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkPolyDataReader.h>
#include <vtkPoints.h>
#include <vtkUnsignedCharArray.h>
#include <vtkFloatArray.h>
#include <vtkDoubleArray.h>
#include <vtkCellArray.h>
#include <vtkDataSetReader.h>
#include <vtkContourFilter.h>
#include <vtkStructuredGrid.h>
#include <vtkArrowSource.h>
#include <vtkGlyph3D.h>
#include <vtkHedgeHog.h>
#include <vtkCubeSource.h>
#include <vtkPlane.h>
#include <vtkCutter.h>
#include <vtkStructuredGridReader.h>
#include <vtkPointSource.h>
#include <vtkRungeKutta4.h>
#include <vtkStreamTracer.h>
#include <vtkLineSource.h>

int main()
{

  vtkSmartPointer<vtkDataSetReader> rdr = vtkSmartPointer<vtkDataSetReader>::New();
  rdr->SetFileName("proj8.vtk");
  rdr->Update();
  vtkDataSet *grad = rdr->GetOutput();
  grad->GetPointData()->SetActiveAttribute("grad", vtkDataSetAttributes::VECTORS);
  rdr->Update();
  //vtkDataSet *hardyglobal = rdr->GetOutput();

  //contour filters for isosurfacing =============================================
  vtkContourFilter *cf1 = vtkContourFilter::New();
  cf1->SetValue(1, 2.5);
  cf1->SetInputConnection(rdr->GetOutputPort());
  cf1->Update();

  vtkContourFilter *cf2 = vtkContourFilter::New();
  cf2->SetValue(1, 5.0);
  cf2->SetInputConnection(rdr->GetOutputPort());
  cf2->Update();

  //Planes and cutters for slices ===================================================
  //plane 1 
  vtkSmartPointer<vtkPlane> plane1 = vtkSmartPointer<vtkPlane>::New();
  plane1->SetOrigin(rdr->GetOutput()->GetCenter());
  plane1->SetNormal(1,0,0);
  //cutter 1
  vtkSmartPointer<vtkCutter> cutter1 = vtkSmartPointer<vtkCutter>::New();
  cutter1->SetCutFunction(plane1);
  cutter1->SetInputConnection(rdr->GetOutputPort());
  cutter1->Update();
  //plane 2
  vtkSmartPointer<vtkPlane> plane2 = vtkSmartPointer<vtkPlane>::New();
  plane2->SetOrigin(rdr->GetOutput()->GetCenter());
  plane2->SetNormal(0,1,0);
  //cutter 2
  vtkSmartPointer<vtkCutter> cutter2 = vtkSmartPointer<vtkCutter>::New();
  cutter2->SetCutFunction(plane2);
  cutter2->SetInputConnection(rdr->GetOutputPort());
  cutter2->Update();
  //plane 3
  vtkSmartPointer<vtkPlane> plane3 = vtkSmartPointer<vtkPlane>::New();
  plane3->SetOrigin(rdr->GetOutput()->GetCenter());
  plane3->SetNormal(0,0,1);
  //cutter 3
  vtkSmartPointer<vtkCutter> cutter3 = vtkSmartPointer<vtkCutter>::New();
  cutter3->SetCutFunction(plane3);
  cutter3->SetInputConnection(rdr->GetOutputPort());
  cutter3->Update();


  //Glyphs ===========================================
  vtkSmartPointer<vtkArrowSource> arrow = vtkSmartPointer<vtkArrowSource>::New();
  vtkSmartPointer<vtkGlyph3D> glyph = vtkSmartPointer<vtkGlyph3D>::New();
  glyph->SetInputData(rdr->GetOutput());
  glyph->SetSourceConnection(arrow->GetOutputPort());
  glyph->SetVectorModeToUseVector();
  glyph->SetColorModeToColorByScalar();
  glyph->SetScaleModeToDataScalingOff();
  glyph->OrientOn();
  glyph->SetScaleFactor(.2);

  //Streamlines ======================================================================
  vtkSmartPointer<vtkLineSource> line = vtkSmartPointer<vtkLineSource>::New();
  line->SetPoint1(-9,0,0);
  line->SetPoint2(9,0,0);
  line->SetResolution(19);
  vtkSmartPointer<vtkRungeKutta4> integ = vtkSmartPointer<vtkRungeKutta4>::New();
  vtkSmartPointer<vtkStreamTracer> streamer = vtkSmartPointer<vtkStreamTracer>::New();
  streamer->SetInputConnection(rdr->GetOutputPort());
  streamer->SetSourceConnection(line->GetOutputPort());
  streamer->SetMaximumPropagation(19);
  //streamer->SetInitialIntegrationStepUnitToCellLengthUnit();
  //streamer->SetInitialIntegrationStep(0.1);
  //streamer->SetIntegrationDirectionToForward();
  streamer->SetIntegrator(integ);
  




  //mappers and actors =====================================================================
  //streamlines
  
  vtkSmartPointer<vtkPolyDataMapper> streamMapper =  vtkSmartPointer<vtkPolyDataMapper>::New();
  streamMapper->SetInputConnection(streamer->GetOutputPort());
  streamMapper->SetScalarRange(grad->GetScalarRange());
  vtkSmartPointer<vtkActor> streamActor = vtkSmartPointer<vtkActor>::New();
  streamActor->SetMapper(streamMapper);
  

  //Cutters ===============================================================================
  //1
  vtkSmartPointer<vtkPolyDataMapper> cutMapper1 = vtkSmartPointer<vtkPolyDataMapper>::New();
  cutMapper1->SetInputConnection(cutter1->GetOutputPort());
  cutMapper1-> SetScalarRange(rdr->GetOutput()->GetScalarRange());
  vtkSmartPointer<vtkActor> cutActor1 = vtkSmartPointer<vtkActor>::New();
  cutActor1->SetMapper(cutMapper1);
  //2
  vtkSmartPointer<vtkPolyDataMapper> cutMapper2 = vtkSmartPointer<vtkPolyDataMapper>::New();
  cutMapper2->SetInputConnection(cutter2->GetOutputPort());
  cutMapper2-> SetScalarRange(rdr->GetOutput()->GetScalarRange());
  vtkSmartPointer<vtkActor> cutActor2 = vtkSmartPointer<vtkActor>::New();
  cutActor2->SetMapper(cutMapper2);
  //3
  vtkSmartPointer<vtkPolyDataMapper> cutMapper3 = vtkSmartPointer<vtkPolyDataMapper>::New();
  cutMapper3->SetInputConnection(cutter3->GetOutputPort());
  cutMapper3-> SetScalarRange(rdr->GetOutput()->GetScalarRange());
  vtkSmartPointer<vtkActor> cutActor3 = vtkSmartPointer<vtkActor>::New();
  cutActor3->SetMapper(cutMapper3);

  //Glyphs ==================================================================================
  vtkSmartPointer<vtkPolyDataMapper> glyphMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
  glyphMapper->SetInputConnection(glyph->GetOutputPort());
  glyphMapper->ScalarVisibilityOn();
  glyphMapper->SetScalarRange(rdr->GetOutput()->GetScalarRange());
  vtkSmartPointer<vtkActor> glyphActor = vtkSmartPointer<vtkActor>::New();
  glyphActor->SetMapper(glyphMapper);

  //Isosurfacing =========================================================================
  vtkSmartPointer<vtkPolyDataMapper> cf1Mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
  cf1Mapper->SetInputConnection(cf1->GetOutputPort());
  vtkSmartPointer<vtkPolyDataMapper> cf2Mapper = vtkSmartPointer<vtkPolyDataMapper>::New();
  cf2Mapper->SetInputConnection(cf2->GetOutputPort());
  //cf1Actor
  vtkSmartPointer<vtkActor> cf1Actor = vtkSmartPointer<vtkActor>::New();
  cf1Actor->GetProperty()->SetColor(0.0,0.0,1.0);
  cf1Actor->SetMapper(cf1Mapper);
  cf1Actor->GetMapper()->ScalarVisibilityOff();
  //cf2Actor
  vtkSmartPointer<vtkActor> cf2Actor = vtkSmartPointer<vtkActor>::New();
  cf2Actor->GetProperty()->SetColor(0.0,1.0,0.0);
  cf2Actor->SetMapper(cf2Mapper);
  cf2Actor->GetMapper()->ScalarVisibilityOff();
  //=====================================================================

  //window to hold all renderers
  vtkSmartPointer<vtkRenderWindow> renderWindow = vtkSmartPointer<vtkRenderWindow>::New();
  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = 
    vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetRenderWindow(renderWindow);

  //renderer #1
  vtkSmartPointer<vtkRenderer> renderer1 = vtkSmartPointer<vtkRenderer>::New();
  renderWindow->AddRenderer(renderer1);
  renderer1->SetViewport(0,0,.5,.5);
  //add stuff to render window
  renderer1->AddActor(cf1Actor);
  renderer1->AddActor(cf2Actor);
  renderer1->ResetCamera();
  renderWindow->Render();
  

  //renderer #2
  vtkSmartPointer<vtkRenderer> renderer2 = vtkSmartPointer<vtkRenderer>::New();
  renderWindow->AddRenderer(renderer2);
  renderer2->SetViewport(0,.5,.5,1);
  //add cube
  renderer2->AddActor(cutActor1);
  renderer2->AddActor(cutActor2);
  renderer2->AddActor(cutActor3);
  renderer2->ResetCamera();
  renderWindow->Render();


  //renderer #3
  vtkSmartPointer<vtkRenderer> renderer3 = vtkSmartPointer<vtkRenderer>::New();
  renderWindow->AddRenderer(renderer3);
  renderer3->SetViewport(.5,0,1,.5);
  //add stuff
  renderer3->AddActor(glyphActor);
  renderer3->ResetCamera();
  renderer3->Render();


  //renderer #4
  vtkSmartPointer<vtkRenderer> renderer4 = vtkSmartPointer<vtkRenderer>::New();
  renderWindow->AddRenderer(renderer4);
  renderer4->SetViewport(.5,.5,1,1);
  //add stuff
  renderer4->AddActor(streamActor);
  renderer2->ResetCamera();
  renderWindow->Render();


  renderWindow->SetSize(600,600);
  renderWindow->Render();
  renderWindow->SetWindowName("Project 8");

  //for cross sections, 3 cutters and 3 planes
 
  renderWindowInteractor->Start();


}
