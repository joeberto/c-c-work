
//#include <image.h>
#include <source.h>
#include <sink.h>

class Image;

#ifndef FILTER_H
#define FILTER_H
class Filter: public Source, public Sink {
	//virtual const char* FilterName() = 0;

};
#endif
class CheckSum: public Sink {
	public:
	void OutputCheckSum(char* fn);
};

class Color: public Source {
	public:
	void Execute();
	Color(int w, int h, int red, int green, int blue);
	
	
};

class Mirror: public Filter {
	public:
	void Execute();
	Mirror();
};

class Rotate: public Filter {
	public:
	void Execute();
	Rotate();
};

class Subtract: public Filter {
	public:
	void Execute();
	Subtract();
};

class Grayscale: public Filter {
	public:
	void Execute();
	Grayscale();
};

class Blur: public Filter {
	public:
	void Execute();
	Blur();
};

#ifndef SHRINKER_H
#define SHRINKER_H
class Shrinker: public Filter{
	public:
	void Execute();
	Shrinker();
	
};
#endif

#ifndef LRCONCAT_H
#define LRCONCAT_H
class LRConcat: public Filter{
	public:
	void Execute();
	LRConcat();
};
#endif

#ifndef TBCONCAT_H
#define TBCONCAT_H
class TBConcat: public Filter{
	public:
	void Execute();
	TBConcat();
			
};
#endif

#ifndef BLENDER_H
#define BLENDER_H
class Blender: public Filter{
	private:
	float factor;
	public:
	void Execute();
	void SetFactor(float f);
	Blender();
};
#endif
