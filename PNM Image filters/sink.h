#include <image.h>


#ifndef SINK_H
#define SINK_H
class Sink{

    protected:
	const Image* img1;
	const Image* img2;
    public:
	Sink(void);
	void SetInput(Image* img1in);
	void SetInput2(Image* img2in);
	//virtual const char* SinkName()=0;
};
#endif
