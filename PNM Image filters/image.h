
//include <filters.h>
class PNMreader;
class Source;
class Filter;

#ifndef PIXEL_H
#define PIXEL_H


class Pixel
{
 public:
  unsigned char r;
  unsigned char g;
  unsigned char b;
};
#endif

#ifndef IMAGE_H
#define IMAGE_H
class Image
{
    public:
	
	//default constructor
	Image(void);
	//parameterized
	Image(int w, int h);
	Image(Source* s);
	//destructor
	~Image();
	//mutators- theselive in image.C
	int GetHeight() const;
	int GetWidth() const;
	void setDimensions(int w, int h);
	void resetSize(int w, int h);
	Pixel* GetPixelList() const;
	void Update() const;	
	//void SetFilter(Filter* f);
	//Filter* GetFilter();
	void SetStatus(bool) const;
	bool GetStatus();
	void SetSource(Source*);
	Source* GetSource();
	

    private:
	//copy
	Image(Image &img);
	//pixel list that is our array of pixels
    	Pixel *pixellist;
	//width and height
	int width;
	int height;
	mutable bool isupdated;
	Source* mysource;
	PNMreader* myreader;
};
#endif
