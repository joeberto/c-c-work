#include <exception>
#include <logging.h>
#include <stdio.h>


DataFlowException::DataFlowException(const char *type, const char *error){
		//take in error message, set to msg,
		//write out type and error msg in logger
		//msg = "Throwing Exception: " type + ": " + error;
		sprintf(msg,"%s: %s: %s","Throwing Exception", type, error);
		Logger::LogEvent(msg);
		
}



//static FILE Logger::logger = NULL;
FILE* Logger::logger = NULL;

void Logger::LogEvent(const char *event){
	
	if(logger == NULL){
		//open logger file
		logger = fopen("logger", "w");
		}

	fprintf(logger, "%s\n", event);
		
	
}

void Logger::Finalize(){
	
	if(logger != NULL){
		//close logger file
		fclose(logger);
	}
}
	
