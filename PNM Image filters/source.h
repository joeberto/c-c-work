#include <image.h>

#ifndef SOURCE_H
#define SOURCE_H

class Source{
	friend class Image;

	private:
		Image output;
	public:
		Source();
		virtual void Update();
		Image* GetOutput();
		//virtual const char* SourceName() = 0;
		
	protected:
		//pure virtual function - any inheriting classes must implement
		virtual void Execute() =0;
		
};
#endif
