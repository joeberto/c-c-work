#include <image.h>
#include <source.h>


#ifndef PNMREADER_H
#define PNMREADER_H
class PNMreader: public Source {

	private:
		char* filename;


   public:

	PNMreader(char* fn);
	~PNMreader();
	virtual void Execute();
};
#endif	
	
