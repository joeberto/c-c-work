
//AUTHORSHIP STATEMENT
//Joseph Hill, jhill12, CIS415 Project 1
//This is my own work.

#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h> 
#include "p1fxns.h"

//node in queue
typedef struct node{
	//each node has a pid_t in it
	pid_t pid;
	// and a pointer to the next node
	struct node* next;
} Node;

//queue thaat has pointers to start and end of queue
typedef struct Queue{
	Node* head;
	Node* tail;
	//node count
	int size;
} Queue;

Queue* createQueue(){
	//malloc queue
	Queue* q = (Queue*)malloc(sizeof(Queue));
	//init all struct vars
	q->head = NULL;
	q->tail = NULL;
	q->size = 0;
	return q;
}
//returns 1 if empty, 0 otherwise
int isEmpty(Queue* q){
 	if (q->size == 0){
 		//queue is empty. return 1;
 		return 1;
 	}
 	return 0;
}

//insert into queue. 
void enqueue(pid_t pid, Queue* q){
	//malloc space for a new node
	Node* newNode = (Node*)malloc(sizeof(Node));
	newNode->pid = pid;
	newNode->next = NULL;
	//if our queue is empty
	if (isEmpty(q)){
		//our new node will be our head and tail
		q->head = newNode;
		q->tail = newNode;
	}
	else{
		//else not empty
		//set tail nodes next to newNode
		q->tail->next = newNode;
		//tail is our newNode
		q->tail = newNode;
	}
	q->size++;
}

//deque an item - returns the value from node
pid_t dequeue(Queue* q){

	pid_t returnval;

	if (isEmpty(q)){
		//trying to dequene an empty queue
		return -1;
	}
	//if queue only has one node
	if (q->size == 1){
		q->size--;
		returnval = q->head->pid;
		free(q->head);
		return returnval;
	}
	//else more than on node
	//temp node to return current head
	Node* oldHead = q->head;
	//set head to current heads next
	q->head = q->head->next;
	//decrease size
	q->size--;
	returnval = oldHead->pid;
	free(oldHead);
	return returnval;
}

void destroyQ(Queue* q){

	if (isEmpty(q)){
		free(q);
		return;
	}
	Node* node = q->head;
	Node* temp;
	while (node != NULL){
		temp = node;
		node = node->next;
		free(temp);
	}
	free(q);
}

int running = 0;
int activePids;
int num_processors;
int num_processes;

//holds all proccesses are running - size will nprocessors big
Queue* running_q; 
//holds all proccesses that are waiting - size will be nprocceses-nprocessors 
Queue* waiting_q; 
//contain each arg that were using to run the program
typedef struct args {

	int envhit;
	int quantum;
	int nprocesses;
	int nprocessors;
	///this is argc for "command"
	int cargc;
	//this is argv for "command"
	char **cargv;

} ParsedArgs;

void pa_destroy(ParsedArgs *pa){
	int i;
	//for each entry in wordlist
	for(i=0; i< pa->cargc; i++){
		//free the memory it points to
		free(pa->cargv[i]);
	}

	//free(pa->wordlist);
	free(pa->cargv);
}


ParsedArgs* ParseArgs(int argc, char * argv[]){

	ParsedArgs* args = (ParsedArgs *)malloc(sizeof(ParsedArgs));
	args->envhit = 0;
	args->quantum = 0;
	args->nprocesses = 0;
	args->nprocessors = 1;
	args->cargc = 0;
	args->cargv = NULL;

	int qhit=0;
	char * env;
	//if calling the env var isnt NULL, set our args data 
	if ( (env = getenv("TH_QUANTUM_MSEC")) != NULL) {
		args->quantum = p1atoi(env);
		args->envhit++;
		qhit++;
		}
		
	//if calling the env var isnt NULL, set our args data 
	if ( (env = getenv("TH_NPROCESSES")) != NULL) {
		args->nprocesses = p1atoi(env);
		args->envhit++;
		
	} 	
	if ( (env = getenv("TH_NPROCESSORS")) != NULL) {
		args->nprocessors = p1atoi(env);
		args->envhit++;
		
	}
	char* p;
	int i;
	for(i=1; i < argc; i++){

		if (p1strneq(argv[i], "--quantum=", 10) == 1){

			args->quantum = p1atoi(argv[i] + 10);
			args->envhit++;
			qhit++;
		}

		if (p1strneq(argv[i], "--number=", 9) == 1){
			
			args->nprocesses = p1atoi(argv[i] + 9);
			args->envhit++;
		
			
		}

		if (p1strneq(argv[i], "--processors=", 13) == 1){

			args->nprocessors = p1atoi(argv[i]+13);
			args->envhit++;
		
			
		}
		if (p1strneq(argv[i], "--command=", 10) == 1){

			p = argv[i]+10;
			//count number of cargs
			while(*p != '\0'){
				if (*p == ' '){
					args->cargc++;
				}
				p++;
			}
			args->cargc++;

			//malloc cargv to point at an array of char pointers
			args->cargv = (char**)malloc(sizeof(char*)*(args->cargc+1));
			args->cargv[0] = argv[i]+10;
			args->cargv[args->cargc] = NULL;

			int k;
			
			int index=10;
			//for each command arg
			for(k=0; k<args->cargc; k++){
				//malloc a word buffer
				char* word = (char*)malloc(sizeof(char)*20);
				index = p1getword(argv[i], index, word);
				args->cargv[k] = word;
				
			}

		}
	}
	//if there is no command OR no env vars 
	if(args->cargv == NULL || args->envhit == 0 || qhit == 0 ){
		p1putstr(1, "ERROR: Improper commands given or no environment variables found");
		p1putstr(1, "Usage: ./thv4 --quantum=<msec> [--number=<nprocesses>] [--processors=<nprocessors>] –-command=’command’");
		p1putstr(1, "\n");
		free(args);
		exit(1);
	}
	return args;
}
void sigHandler(int signum){

	int i;
	pid_t qPid;
	switch (signum){

		case SIGUSR1:
			running = 1;
			break;
		//sig is alarm, time to stop running processes in running
		case SIGALRM:
			//if we have less processes than processors
			//remove nprocessors from running queue
			for(i=0; i< running_q->size; i++){
				qPid = dequeue(running_q);
				//stop the process 
				kill(qPid, SIGSTOP);
				//put it into waiting_q
				enqueue(qPid, waiting_q);
			}
			int min = num_processors;
			if (num_processors > num_processes){
				min = num_processes;
				}
			//pop nprocessors from waiting to run
			for(i=0; i<min; i++){
				qPid = dequeue(waiting_q);
				//start the process
				kill(qPid, SIGCONT);
				//put it into the running_q
				enqueue(qPid, running_q);
			}
	}
}
	//NEED TO DEFINE CHILD HANDLER
		//SIGCHLD HAS A PID
		//IF DEQUEUED PID == SIGCHLD PID
		//REMOVE IT FROM QUEUE BY PUSHING ALL BUT THAT INTO A TEMP QUEUE
		//THEN ALL THAT BACK INTO RUNNING QUEUE
void childHandler(int signum){
	
	if (signum == SIGCHLD){
	int i;
	pid_t rPid = NULL;
	pid_t dPid = NULL;
	Queue* tempQ = createQueue();

	//get dead pid(s)
	while((dPid = waitpid(-1, NULL, WNOHANG)) > 0){
		//decrement active pids count
		//still running processes
		if(dPid == 0){
			free(tempQ);
			return;
		}
		//check if no error on wait
		if(dPid == -1){
			free(tempQ);
			return;
		}

		activePids--;
		for(i=0; i<running_q->size; i++){
			//get running pid

			rPid = dequeue(running_q);
			//if it is not the dead pid, enqueue it into temp queue
			if (dPid != rPid){
				enqueue(rPid, tempQ);
			}
		}
		//put temp queue back into running queue
		for(i=0; i<tempQ->size; i++){
			enqueue(dequeue(tempQ), running_q);
		}
	}
	destroyQ(tempQ);
	return;
}
}

int main( int argc, char * argv[]){

	
	//parse input
	ParsedArgs* input = ParseArgs(argc, argv);
	//set active processes
	activePids = input->nprocesses;
	num_processors = input->nprocessors;
	num_processes = input->nprocesses;

	running_q = createQueue();
	waiting_q = createQueue();

	struct itimerval qAlarm;
	qAlarm.it_value.tv_sec = (input->quantum/1000);
	qAlarm.it_value.tv_usec = (input->quantum*1000)%1000000;
	//this will send a SIGALRM every quantum ms
	qAlarm.it_interval = qAlarm.it_value;
	//qAlarm.it_interval.tv_sec = (input->quantum/1000);
	//qAlarm.it_interval.tv_usec = (input->quantum*1000)%1000000;

	//start time
	struct timeval stop, start;
	gettimeofday(&start, NULL);
	
	int i;
	pid_t pid;

	//this is where we setup our signal handler(s)
	//put one of SIGCHILD here?
	signal(SIGUSR1, sigHandler);
	signal(SIGCHLD, childHandler);

	for (i=0; i<input->nprocesses; i++){

		pid = fork();

		if (pid==0){
			while (running==0){
				pause();
			}
			if (execvp(input->cargv[0], input->cargv) == -1){
				p1putstr(2, "Error: execvp failure");
				p1putstr(2, "\n");
				exit(-1);
			}
		}
		
		//if fork fails
		if (pid < 0){
			//free memory 
			pa_destroy(input);
			free(input);
			p1putstr(2, "Error: fork failure");
			p1putstr(2, "\n");
			exit(1);
		} 

		else{
			//push pid onto waiting w
			enqueue(pid, waiting_q);
		}

	}

	//done forking all processes
	gettimeofday(&start, NULL);
	//code above this is all children
	//all signals need to be sent from parent
	pid_t tempPid;

	for(i=0; i<input->nprocesses; i++){
		//sends signal to each child
		tempPid = dequeue(waiting_q);
		kill(tempPid, SIGUSR1);
		enqueue(tempPid, waiting_q);
	}
	sleep(1);
	for(i=0; i<input->nprocesses; i++){
		//stop the processes in the waiting queue
		tempPid = dequeue(waiting_q);
		kill(tempPid, SIGSTOP);
		enqueue(tempPid, waiting_q);
	}
	//the loop below runs on the floor of nproccesors and nprocesses
	int min = input->nprocessors;
	if (input->nprocessors > input->nprocesses){
		min = input->nprocesses;
	}

	for(i=0; i<min; i++){
		//put proccesses from waiting into running
		enqueue(dequeue(waiting_q), running_q);
		
	}

	for(i=0; i<running_q->size; i++){
		//continue the processes
		//only send this to ones in the running queue
		tempPid = dequeue(running_q);
		kill(tempPid, SIGCONT);
		enqueue(tempPid, running_q);
	}

	signal(SIGALRM, sigHandler);

	//set itimerval
	setitimer(ITIMER_REAL, &qAlarm, NULL);


	while(activePids){
		pause();
	}

	gettimeofday(&stop, NULL);
	int seconds = (stop.tv_sec - start.tv_sec);
	int mseconds = (stop.tv_sec - start.tv_sec) * 1000 + (stop.tv_usec - start.tv_usec)/1000;

	
	//buffer for printing.
	char* printbuf = (char*)malloc(sizeof(char)*30);
	char pb[4]="";

	//print time
	p1putstr(1, "The elapsed time to execute ");
	p1itoa(input->nprocesses, printbuf);
	p1putstr(1, printbuf);
	p1putstr(1, " copies of \"");
	p1putstr(1, input->cargv[0]);
	p1putstr(1, "\" on ");
	p1itoa(input->nprocessors, printbuf);
	p1putstr(1, printbuf);
	p1putstr(1, " processors is ");
	p1itoa(seconds, printbuf);
	p1putstr(1, printbuf);
	p1putstr(1, ".");

	p1itoa(mseconds, printbuf);
	p1strpack(printbuf,3,'0',pb);
	p1putstr(1, pb);
	p1putstr(1, "\n");

	//free memory
	destroyQ(running_q);
	destroyQ(waiting_q);
	pa_destroy(input);
	free(input);
	free(printbuf);
	return 0;
}

