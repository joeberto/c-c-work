
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h> 
#include "p1fxns.h"

//node in queue
typedef struct node{
	//each node has a pid_t in it
	int pid;
	// and a pointer to the next node
	struct node* next;
} Node;

//queue thaat has pointers to start and end of queue
typedef struct Queue{
	Node* head;
	Node* tail;
	//node count
	int size;
} Queue;

Queue* createQueue(){
	//malloc queue
	Queue* q = (Queue*)malloc(sizeof(Queue));
	//init all struct vars
	q->head = NULL;
	q->tail = NULL;
	q->size = 0;
	return q;

}
//insert into queue. 
void enqueue(int pid, Queue* q){
	//malloc space for a new node
	Node* newNode = (Node*)malloc(sizeof(Node));
	newNode->pid = pid;
	newNode->next = NULL;
	//if our queue is empty
	if (isEmpty(q)){
		//our new node will be our head and tail
		q->head = newNode;
		q->tail = newNode;
	}
	else{
		//else not empty
		//set tail nodes next to newNode
		q->tail->next = newNode;
		//tail is our newNode
		q->tail = newNode;
	}
	q->size++;
}

//deque an item - returns the value from node
int dequeue(Queue* q){
	int returnval;

	if (isEmpty(q)){
		return;
	}
	//if queue only has one node
	if (q->size == 1){
		q->size--;
		returnval = q->head->pid;
		free(q->head);
		return returnval;
	}
	//else more than on node
	//temp node to return current head
	Node* oldHead = q->head;
	//set head to current heads next
	q->head = q->head->next;
	//decrease size
	q->size--;
	returnval = oldHead->pid;
	free(oldHead);
	return returnval;
}
//returns 1 if empty, 0 otherwise
int isEmpty(Queue* q){
 	if (q->size == 0){
 		//queue is empty. return 1;
 		return 1;
 	}
 	return 0;
}

int main(int argc, char * argv[]){


	Queue* q = createQueue();
	
	enqueue(1,q);
	enqueue(2,q);
	dequeue(q);
	dequeue(q);
	enqueue(3,q);
	enqueue(4,q);
	dequeue(q);
	enqueue(5,q);
	dequeue(q);
	dequeue(q);

	free(q);

	return 0;

}