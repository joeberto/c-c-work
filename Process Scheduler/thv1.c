//AUTHORSHIP STATEMENT
//Joseph Hill, jhill12, CIS415 Project 1
//This is my own work.

#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h> 
#include "p1fxns.h"


//contain each arg that were using to run the program
typedef struct args {

	int envhit;
	int quantum;
	int nprocesses;
	int nprocessors;
	///this is argc for "command"
	int cargc;
	//this is argv for "command"
	char **cargv;
	//this is we can free memory later
	//char** wordlist;

} ParsedArgs;

void pa_destroy(ParsedArgs *pa){
	int i;
	//for each entry in wordlist
	for(i=0; i< pa->cargc; i++){
		//free the memory it points to
		free(pa->cargv[i]);
	}

	//free(pa->wordlist);
	free(pa->cargv);
}



ParsedArgs* ParseArgs(int argc, char * argv[]){

	ParsedArgs* args = (ParsedArgs *)malloc(sizeof(ParsedArgs));
	args->envhit = 0;
	args->quantum = 0;
	args->nprocesses = 0;
	args->nprocessors = 1;
	args->cargc = 0;
	args->cargv = NULL;
	//args->wordlist = NULL;


	char * env;

	//if calling the env var isnt NULL, set our args data 
	if ( (env = getenv("TH_QUANTUM_MSEC")) != NULL) {
		args->quantum = p1atoi(env);
		args->envhit++;
		}
		
	//if calling the env var isnt NULL, set our args data 
	if ( (env = getenv("TH_NPROCESSES")) != NULL) {
		args->nprocesses = p1atoi(env);
		args->envhit++;

	} 	
	if ( (env = getenv("TH_NPROCESSORS")) != NULL) {
		args->nprocessors = p1atoi(env);
		args->envhit++;

	}

	int i;
	for(i=1; i < argc; i++){

		if (p1strneq(argv[i], "--quantum=", 10) == 1){
			args->quantum = p1atoi(argv[i] + 10);
			args->envhit++;
		}

		if (p1strneq(argv[i], "--number=", 9) == 1){
			
			args->nprocesses = p1atoi(argv[i] + 9);
			args->envhit++;
		}


		if (p1strneq(argv[i], "--processors=", 13) == 1){

			args->nprocessors = p1atoi(argv[i]+13);
			args->envhit++;
		}

		if (p1strneq(argv[i], "--command=", 10) == 1){

			char *p = argv[i]+10;
			//count number of cargs
			while(*p != '\0'){
				if (*p == ' '){
					args->cargc++;
				}
				p++;
			}
			args->cargc++;

			//malloc cargv to point at an array of char pointers
			args->cargv = (char**)malloc(sizeof(char*)*(args->cargc+1));
			args->cargv[0] = argv[i]+10;
			args->cargv[args->cargc] = NULL;

			int k;
			
			int index=10;
			//for each command arg
			for(k=0; k<args->cargc; k++){
				//malloc a word buffer
				char* word = (char*)malloc(sizeof(char)*20);
				index = p1getword(argv[i], index, word);
				args->cargv[k] = word;
				
			}

		}
	}
	//if there is no command OR no env vars 
	if(args->cargv == NULL || args->envhit == 0 ){
		p1putstr(1, "ERROR: No command given or no environment variables found");
		p1putstr(1, "\n");
		free(args);
		exit(1);
	}
	return args;
}
	//if anything in args is not set, throw error


int main( int argc, char * argv[]){

	//parse input
	ParsedArgs* input = ParseArgs(argc, argv);

	//start time
	struct timeval stop, start;
	gettimeofday(&start, NULL);
	
	//array to hold PIDs
	pid_t* pid = (pid_t*)malloc(sizeof(pid_t)*input->nprocesses);
	if (pid == NULL){
		pa_destroy(input);
		free(input);
	 	p1putstr(1, "Memory allocation failure");
	 	p1putstr(1, "\n");
		exit(1);
	}
	int i;

	for (i=0; i<input->nprocesses; i++){
		//get pid 
		pid[i] = fork();
		
		if (pid[i]==0){
			execvp(input->cargv[0], input->cargv);
		}
		//if fork fails
		if (pid[i] < 0){
			//free memory 
			pa_destroy(input);
			free(input);
			free(pid);
			exit(1);
		} 
	}
	for(i=0; i< input->nprocesses; i++){
		wait(&pid[i]);
	}

	//end time
	gettimeofday(&stop, NULL);
	int seconds = (stop.tv_sec - start.tv_sec);
	int mseconds = (stop.tv_sec - start.tv_sec) * 1000 + (stop.tv_usec - start.tv_usec)/1000;

	
	//buffer for printing.
	char* printbuf = (char*)malloc(sizeof(char)*30);
	//print time
	p1putstr(1, "The elapsed time to execute ");
	p1itoa(input->nprocesses, printbuf);
	p1putstr(1, printbuf);
	p1putstr(1, " copies of \"");
	p1putstr(1, input->cargv[0]);
	p1putstr(1, "\" on ");
	p1itoa(input->nprocessors, printbuf);
	p1putstr(1, printbuf);
	p1putstr(1, " processors is ");
	p1itoa(seconds, printbuf);
	p1putstr(1, printbuf);
	p1putstr(1, ".");
	p1itoa(mseconds, printbuf);
	p1putstr(1, printbuf+1);
	p1putstr(1, "\n");

	//free memory
	pa_destroy(input);
	free(input);
	free(printbuf);
	free(pid);
	return 0;
}