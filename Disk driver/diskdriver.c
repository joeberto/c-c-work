//AUTHORSHIP STATEMENT
//Joe Hill, jhill12, CIS 415 Project 2
//This is my own work.

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include "sectordescriptor.h"
#include "sectordescriptorcreator.h"
#include "block.h"
#include "pid.h"
#include "freesectordescriptorstore.h"
#include "freesectordescriptorstore_full.h"
#include "diskdevice.h"
#include "voucher.h"
#include "BoundedBuffer.h"
#include "diagnostics.h"

//to get rid of warnings on unsued variabls (threads)
#define UNUSED(x) (void)(x)
//max size of buffer
#define MAXBUFF 100
//number of vouchers
#define VOUCHSIZE 100
//bounded buffers to hold sd
BoundedBuffer* readBuf;
BoundedBuffer* writeBuf;
//bounded buff to hold vouchers
BoundedBuffer* vouchBuf;
//threads for reading and writing
pthread_t readingThread;
pthread_t writingThread;
//global pointers 
DiskDevice* gDD;
FreeSectorDescriptorStore* gFSDS;


struct voucher{
	//SD associated with voucher
	SectorDescriptor* vsd;
	//status of the call on the SD
	int status;
	//0 for read, 1 for write
	int rw;
	//mutex for this voucher to keep thread safe
	pthread_mutex_t vmutex;
	//condition var for signaling
	pthread_cond_t vcond;
};

//declaration of VOUCHSIZE vouchers
Voucher myVouchers[VOUCHSIZE];

void initVouchers(){
	//create a BB with size of #vouchers
	vouchBuf = createBB(VOUCHSIZE);
	//temp voucher ptr
	Voucher* v;
	int i;
	//for each voucher, init and add to buffer
	for(i=0; i<VOUCHSIZE; i++){
		v = &myVouchers[i];
		v->vsd = NULL;
		v->status = -1;
		v->rw = -1;
		pthread_mutex_init(&(v->vmutex), NULL);
	    pthread_cond_init(&(v->vcond), NULL);
		blockingWriteBB(vouchBuf, v);
	}
}

void* readRun(void* vp){
	//surpress gcc warnings
	UNUSED(vp);
	Voucher* v;

	while(1){
		//blocks until a voucher is available
		v = blockingReadBB(readBuf);
		//lock to not interfere with redeem voucher
		pthread_mutex_lock(&(v->vmutex));
		//set vouchers status to that of read sector
		v->status = read_sector(gDD, v->vsd);
		//once were done reading, safe to unlock (safe for redeem voucher)
		pthread_mutex_unlock(&(v->vmutex));
		//signal that vouch is done
		pthread_cond_signal(&(v->vcond));
	}
}


void* writeRun(void* vp){
	//supress gcc warnings
	UNUSED(vp);
	Voucher* v;

	while(1){
		//blocks until a voucher is available
		v = blockingReadBB(writeBuf);
		//lock to not iterfere with redeem voucher
		pthread_mutex_lock(&(v->vmutex));
		//set vouchers status to that of write sector
		v->status = write_sector(gDD, v->vsd);
		//once were done writing, safe to unlock (safe for redeem voucher)
		pthread_mutex_unlock(&(v->vmutex));
		//signal that vouch is done
		pthread_cond_signal(&(v->vcond));

	}
}

void printDiagnostics(Voucher *v){
	//if it is type read
	if (v->rw == 0){
		//if it was succesfull
		if (v->status == 1){
			DIAGNOSTICS("[DRIVER> %i successful read of sector %i from disk\n", 
						(int)sector_descriptor_get_pid(v->vsd), (int)sector_descriptor_get_block(v->vsd));
		}
		//else it was failure
		else{
			DIAGNOSTICS("[DRIVER> %i failed read of sector %i from disk\n", 
						(int)sector_descriptor_get_pid(v->vsd), (int)sector_descriptor_get_block(v->vsd));
		}
	}
	else if (v->rw == 1){
	//if it is type write
		//if it was successful
		if(v->status == 1){
			DIAGNOSTICS("[DRIVER> %i successful write of sector %i from disk\n", 
						(int)sector_descriptor_get_pid(v->vsd), (int)sector_descriptor_get_block(v->vsd));
		}
		//else it was failure
		else{
			DIAGNOSTICS("[DRIVER> %i failed write of sector %i from disk\n", 
						(int)sector_descriptor_get_pid(v->vsd), (int)sector_descriptor_get_block(v->vsd));

		}
	}
}

void init_disk_driver(DiskDevice *dd, void *mem_start, unsigned long mem_length,
		      FreeSectorDescriptorStore **fsds){

	//init vouchers
	initVouchers();

	/* create Free Sector Descriptor Store */
	if((*fsds = create_fsds()) == NULL){
		//failure. return.
		return;
	}
	//global variable
	gFSDS = *fsds;

/* load FSDS with packet descriptors constructed from mem_start/mem_length */
	create_free_sector_descriptors(*fsds, mem_start, mem_length);
	//gloabl disk device
	gDD = dd;

/* create any buffers required by your thread[s] */
	if((readBuf = createBB(MAXBUFF)) == NULL){
		//failure, cleanup
		free(fsds);
		return;
	}
	if((writeBuf = createBB(MAXBUFF)) == NULL){
		//failue, cleanup
		free(fsds);
		free(readBuf);
		return;
	}

	//create reading thread
	if((pthread_create(&readingThread, NULL, readRun, NULL)) != 0){
		//failure, cleanup
		free(fsds);
		free(readBuf);
		free(writeBuf);
		return;
	}

	//create writing thread
	if((pthread_create(&writingThread, NULL, writeRun, NULL)) != 0){
		//failure, cleanup
		free(fsds);
		free(readBuf);
		free(writeBuf);
		return;
	}
}

/*
 * the following calls are used to write a sector to the disk
 * the nonblocking call must return promptly, returning 1 if successful at
 * queueing up the write, 0 if not (in case internal buffers are full)
 * the blocking call will usually return promptly, but there may be
 * a delay while it waits for space in your buffers.
 * neither call should delay until the sector is actually written to the disk
 * for a successful nonblocking call and for the blocking call, a voucher is
 * returned that is required to determine the success/failure of the write
 */
void blocking_write_sector(SectorDescriptor *sd, Voucher **v){
	//get voucher from buffer
	Voucher* vouch = blockingReadBB(vouchBuf);
	//set pointer to sd
	vouch->vsd = sd;
	//set voucher rw to 1 so we know its writing
	vouch->rw = 1;
	//set status of vouch to undetermined (-1)
	vouch->status = -1;
	//point to pointer 
	*v = vouch;
	//blocking add to BB. Returns once written
	blockingWriteBB(writeBuf, *v);

}
int nonblocking_write_sector(SectorDescriptor *sd, Voucher **v){
	//if you are able to queue up sector descriptor immediately
    //return a Voucher through *v and return 1 
    //otherwise, return 0 */
    //temp pointer
    Voucher* vouch = NULL;
    if ((nonblockingReadBB(vouchBuf, (void**)&vouch)) == 0){
    	//if no voucher avaiable, return 0
    	return 0;
    }
	//set pointer to sd
	vouch->vsd = sd;
	//set voucher rw to 1 so we know its writing
	vouch->rw = 1;
	//set status of vouch to undetermined (-1)
	vouch->status = -1;
	//point to pointer 
	*v = vouch;
	//non blokcing add to BB. returns status of write
	return nonblockingWriteBB(writeBuf, *v);

}

/*
 * the following calls are used to initiate the read of a sector from the disk
 * the nonblocking call must return promptly, returning 1 if successful at
 * queueing up the read, 0 if not (in case internal buffers are full)
 * the blocking callwill usually return promptly, but there may be
 * a delay while it waits for space in your buffers.
 * neither call should delay until the sector is actually read from the disk
 * for successful nonblocking call and for the blocking call, a voucher is
 * returned that is required to collect the sector after the read completes.
 */
void blocking_read_sector(SectorDescriptor *sd, Voucher **v){
	/* queue up sector descriptor for reading */
/* return a Voucher through *v for eventual synchronization by application */
/* do not return until it has been successfully queued */

	//get voucher from buffer
	Voucher* vouch = blockingReadBB(vouchBuf);
	//set pointer to sd
	vouch->vsd = sd;
	//set voucher rw to 0 for read
	vouch->rw = 0;
	//set status of vouch to undetermined (-1)
	vouch->status = -1;
	//point to pointer 
	*v = vouch;
	//add to buffer, waiting until there is space if needed.
	blockingWriteBB(readBuf, *v);


}
int nonblocking_read_sector(SectorDescriptor *sd, Voucher **v){
// if you are able to queue up sector descriptor immediately
// return a Voucher through *v and return 1 
// otherwise, return 0 */
	Voucher* vouch = NULL;
	 if ((nonblockingReadBB(vouchBuf, (void**)&vouch)) == 0){
    	//if no voucher avaiable, return 0
    	return 0;
    }
	//set pointer to sd
	vouch->vsd = sd;
	//set voucher rw to 0 so we know its read
	vouch->rw = 0;
	//set status of vouch to undetermined (-1)
	vouch->status = -1;
	//point to pointer 
	*v = vouch;
	//attempt to add to buffer.
	return nonblockingWriteBB(readBuf, *v);


}

/*
 * the following call is used to retrieve the status of the read or write
 * the return value is 1 if successful, 0 if not
 * the calling application is blocked until the read/write has completed
 * if a successful read, the associated SectorDescriptor is returned in *sd
 */
int redeem_voucher(Voucher *v, SectorDescriptor **sd){


	//if vouchers status is -1, read/write has not finished yet
	if (v->status == -1){
		//wait until condition is broadcasted signaling voucher is done
		pthread_cond_wait(&(v->vcond), (&v->vmutex));
	}

	if (v->rw == 0){
		//sd was read, return in passed in SD
		*sd = v->vsd;
	}
	if (v->rw == 1){
		//sd was write, return to FSDS
		blocking_put_sd(gFSDS, v->vsd);
	}
	int status = v->status;
	//unlock voucher
	//pthread_mutex_unlock(&(v->vmutex));
	//put back the voucher into buffer
	blockingWriteBB(vouchBuf, v);
	//print diagnostics
	enable_diagnostics();
	printDiagnostics(v);
	//return status of SD
	return status;

}

