#include "mentry.h"
#include "mlist.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define HASH 37

//a bucket for the list - a table entry
struct bucket{ 
	//points to next entry in chain
	struct bucket *next;
	//pointer to the MEntry
	const MEntry *bme;
};
typedef struct bucket Bucket;

//table that holds pointers to pointers of buckets
struct hashtable {
	int size;
	Bucket **table;
};

typedef struct hashtable Hashtable;

/*
 * add - adds a new MEntry to the list;
 * returns 1 if successful, 0 if error (malloc)
 * returns 1 if it is a duplicate
 */
int ml_add(const MList *ml, const MEntry *me){
	unsigned long key = me->hash(me, HASH);
	Bucket *bucket;
	Hashtable *ht = (Hashtable *)ml->self;

	//check if duplicate
	if(ml->lookup(ml,me) != NULL){
		//entry already exists return 1
		return 1;
	}
	//malloc space for a bucket pointer
	bucket = (Bucket *)malloc(sizeof(*bucket));
	//check if succesfull malloc'd
	if (bucket == NULL ){
		return 0;
	}
	bucket->bme = me;
	bucket->next = ht->table[key];
	ht->table[key] = bucket;
	return 1;



}

/*
 * lookup - looks for MEntry in the list, returns matching entry or NULL
 */
const MEntry *ml_lookup(const MList *ml, const MEntry *me){
	//compute the hash for the MEntry
	unsigned long key = me->hash(me, HASH);
	Bucket *bucket;
	Hashtable *ht = (Hashtable *)ml->self;

	bucket = ht->table[key];

	//while the bucket is not NULL and entries arent matched
	while(bucket != NULL && bucket->bme != NULL && me->compare(me, bucket->bme) != 0){
		//keep lookig for that entry
		bucket = bucket->next;

	} 
	//check if anything is there
	if (bucket == NULL || bucket->bme == NULL){
		return NULL;
	}
	//return the entry
	return bucket->bme;



	}



/*
 * destroy - destroy the mailing list
 */
void ml_destroy(const MList *ml){

}

static MList template = {
    NULL, ml_add, ml_lookup, ml_destroy
};



//MList - create a new mailing list; if unsuccessful, return NULL
const MList *MList_create(void){
	//build a fixed-size hash table to hold MEntry instances
	int i;
	int size = 101;
	MList *ml;
	Hashtable *hashtable;

	//malloc MList
	ml = (MList *)malloc(sizeof(MList));
	if (ml == NULL){
		return NULL;
	}
	hashtable = (Hashtable *)malloc(sizeof(Hashtable));
 	if(hashtable == NULL) {
		return NULL;
	}
	// Allocate pointers to the buckets. 
	if((hashtable->table = malloc(sizeof(Bucket *)*size)) == NULL ) {
		return NULL;
	}
	for(i = 0; i < size; i++ ) {
		hashtable->table[i] = NULL;
	}
	hashtable->size = size;
	*ml = template;
	ml->self = hashtable;
	return ml;
}


