#include "mentry.h"
#include "mlist.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define HASH 37

/* 	AUTHORSHIP STAEMENT:
Joseph Hill
jhill12
CIS 415 Project 0
This is my own work, with the hash table implementation 
	coming from The C Programming Language book
*/

//a node for the list - a table entry
struct node{ 
	//points to next entry in chain
	struct node *next;
	//pointer to the MEntry
	const MEntry *bme;
};
typedef struct node Node;

//table that holds pointers to pointers of nodes
struct hashtable {
	int size;
	Node **table;
};

typedef struct hashtable Hashtable;

/*
 * add - adds a new MEntry to the list;
 * returns 1 if successful, 0 if error (malloc)
 * returns 1 if it is a duplicate
 */
int ml_add(const MList *ml, const MEntry *me){
	unsigned long key = me->hash(me, HASH);
	//printf("%s\n", "in add");
	//printf("%lu\n", key);
	Node *node;
	Hashtable *ht = (Hashtable *)ml->self;

	//check if duplicate
	if(ml->lookup(ml,me) != NULL){
		//entry already exists return 1
		return 1;
	}
	//malloc space for a node pointer
	node = (Node *)malloc(sizeof(*node));
	//check if succesfull malloc'd
	if (node == NULL ){
		return 0;
	}
	node->bme = me;
	node->next = ht->table[key];
	ht->table[key] = node;
	return 1;



}

/*
 * lookup - looks for MEntry in the list, returns matching entry or NULL
 */
const MEntry *ml_lookup(const MList *ml, const MEntry *me){
	//compute the hash for the MEntry
	unsigned long key = me->hash(me, HASH);
	Node *node;
	Hashtable *ht = (Hashtable *)ml->self;

	//retrieve the node stored at keys location in the table
	for (node = ht->table[key]; node != NULL; node = node->next){
		if ( me->compare(me, node->bme) == 0){
			//duplicate found
			return node->bme;
		}
	}
	//entry not found
	return NULL;
	}
/*
 * destroy - destroy the mailing list
 */
void ml_destroy(const MList *ml){
	//point to hashtable
	Hashtable *ht = (Hashtable *)ml->self;
	//free each node
	int i;
	int size = ht->size;
	//curent node
	Node *cn;
	//next node
	Node *nn;
	//for each index in table
	for(i = 0; i < size; i++ ){
		//if that contains a node
		if (ht->table[i] != NULL){
			//set nn to next node
			nn = ht->table[i]->next;
			//set cn to current node
			cn = ht->table[i];
			//if its the only node, free then continue with loop
			if (nn == NULL){
				free(cn);
				continue;
			}
			//else its not the only node
			//while next node is not null
			while (nn != NULL){
				//set next node (could be NULL)
				nn = cn->next;
				free(cn);
				cn = nn;
			}
		}
	}
	//free table
	free (ht);
	//free mlist
	free((void *)ml);

}

static MList template = {
    NULL, ml_add, ml_lookup, ml_destroy
};



//MList - create a new mailing list; if unsuccessful, return NULL
const MList *MList_create(void){
	//build a fixed-size hash table to hold MEntry instances
	int i;
	int size = 10000000;
	MList *ml;
	Hashtable *hashtable;

	//malloc MList
	ml = (MList *)malloc(sizeof(MList));
	if (ml == NULL){
		return NULL;
	}
	hashtable = (Hashtable *)malloc(sizeof(Hashtable));
 	if(hashtable == NULL) {
 		free((void *)ml);
		return NULL;
	}
	// Allocate pointers to the nodes. 
	if((hashtable->table = malloc(sizeof(Node *)*size)) == NULL ) {
		free(hashtable);
		free((void *)ml);
		return NULL;
	}
	for(i = 0; i < size; i++ ) {
		hashtable->table[i] = NULL;
	}
	hashtable->size = size;
	*ml = template;
	ml->self = hashtable;
	return ml;
}

