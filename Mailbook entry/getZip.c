#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

//function to extract zip code from line
char * getZip(char *from){
    char *p = from;
    int length = 0;
    int spaces = 0;
    //after we reach a , count two spaces
    while (*p != ','){
        p++;
    }
    while (spaces != 2){
        //if (isspace(*p))
        if( *p == ' '){
            spaces++;
        }
        p++;
    }
    char* return_string = (char *)malloc(sizeof(char)*5);
    strncpy(return_string, p, 5);
    //for testing
    //printf(return_string);
    return return_string;
}
int main(){
char line[] = "New York, NY 10012";

getZip(line);
}
