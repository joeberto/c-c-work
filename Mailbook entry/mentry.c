#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include "mentry.h"
#define MAXLINE 4096

/* AUTHORSHIP STATEMENT
Joseph Hill
jhill12
CIS 415 Project 0
This is my own work.
*/
    
//struct to hold entry data
typedef struct me_data {
    char *nameline;
    char *addressline;
    char *locationline;

    char *surname; //compare
    char *housenumber; //compare
    char *zipcode; //compare
} MEdata;

//function to extract zip code from line
char * getZip(char *from){
    //after first comma, 
    char *p = from;
    //while p isnt a number
    while (*p < 48 || *p > 57 ){
        p++;
    }
    char* return_string = (char *)malloc(sizeof(char)*6);
    strncpy(return_string, p, 5);
    return_string[5] = '\0';

    return return_string;
}

//function to copy a string until a seperator char
char * createAndcopyUntil(char *from, char sep){
    char *p = from;
    int length = 0;
     
    while (*p != sep  ) {
        if (*p == '\0'){
            return NULL;
        }
        p++;
        length++;
    }
    
    char *return_string = (char *)malloc(sizeof(char)*(length+1));
    strncpy(return_string, from, length);
    return_string[length] = '\0';

    return return_string;
}

//computes a hash of the MEntry, mod `size'
static unsigned long me_hash(const MEntry *me, unsigned long size){
    //get data from MEntry
    MEdata *d = (MEdata *)me->self;
   
    unsigned long hashval;
    char *s = d->surname;
    for (hashval = 0; *s != '\0'; s++){
        hashval = *s + 31 * hashval;
    }
    s = d->zipcode;
    for (hashval = 0; *s != '\0'; s++){
        hashval = *s + 31 * hashval;
    }
    s = d->housenumber;
    for (hashval = 0; *s != '\0'; s++){
        hashval = *s + 31 * hashval;
    }
 
    return hashval % (size-1);

}

//prints the full address on `fd'
static void me_print(const MEntry *me, FILE *fd){

    MEdata *d = (MEdata *)me->self;
    fprintf(fd, "%s\n%s\n%s\n", d->nameline, d->addressline, d->locationline);
}

 //compares two mail entries, returning <0, 0, >0 if
// me1 < me2, me1 == me2, me1 > me2
static int me_compare(const MEntry *me1, const MEntry *me2){
    //data for entry1
    MEdata *d1 = (MEdata *)me1->self;
    //data fir entry2
    MEdata *d2 = (MEdata *)me2->self;
    //dupicates if they have identical surnames, zip codes, and house numbers
    //sort  by last name
    //check if duplicate
    if (d1->surname == d2->surname && d1->zipcode == d2->zipcode 
        && d1->housenumber == d2->housenumber){
        return 0;

    }//compare surnames
    if (strcasecmp(d1->surname, d2->surname) < 0){
        //d1 is smaller than d2
        return -1;
    }
    if (strcasecmp(d1->surname, d2->surname) == 0){
        //same last name. compare zip
        if (strcasecmp(d1->zipcode, d2->zipcode) < 0 ){
            return -1;
        }
        if (strcasecmp(d1->zipcode, d2->zipcode) == 0){
            //same zip, compare house number
            if(strcasecmp(d1->housenumber, d2->housenumber) < 0){
                return -1;
            }
            if(strcasecmp(d1->housenumber, d2->housenumber) == 0){
                return 0;
            }
            if(strcasecmp(d1->housenumber, d2->housenumber) > 0){
                return 1;
            }
        }
        if (strcasecmp(d1->zipcode, d2->zipcode) > 0){
            return 1;
        }
     }
    if (strcasecmp(d1->surname, d2->surname) > 0){
        return 1;
    }
    return  1;
}
//destroys the mail entry
static void me_destroy(const MEntry *me){
    MEdata *d = (MEdata *)me->self;
    free(d->surname);
    free(d->zipcode);
    free(d->housenumber);
    free(d->nameline);
    free(d->addressline);
    free(d->locationline);
    free(d);
    free((void *)me);
}

static MEntry template = {
    NULL, me_hash, me_print, me_compare, me_destroy
};


//a constructor that obtains the next MEntry from `fd', returns NULL if end of file
const MEntry *MEntry_get(FILE *fd){
    //allocate memory on the heap for a MEntry
    MEntry *me = (MEntry *)malloc(sizeof(MEntry));

    if (me != NULL) {
        MEdata *d = (MEdata *)malloc(sizeof(MEdata));

        if (d != NULL){
            //now must read in from a file use fgets to get a line
            //int i = 0, result;
            char buf[MAXLINE];
            //char *p;
            int count = 0;

            //init all of data to NULL 
            d->nameline = d->addressline = d->locationline = NULL;
            d->surname = d->zipcode = d->housenumber = NULL;

           
            for (count = 1; count <=3; count++){
            //make sure line isnt NULL
            if (fgets(buf, MAXLINE, fd) == NULL){
                //if this fails, still need to fee d's
                //previous data members
                if (count > 1){
                    free(d->nameline);
                    free(d->surname);
                    if(count > 2){
                        free(d->addressline);
                        free(d->housenumber);
                    }
                }

                free(d);
                free((void*)me);
                return NULL;

            }
            buf[strcspn(buf, "\r\n")] = 0;

            //printf("%s%i\n", "count is: ", count);

            switch (count % 3 ) {
                
                case 1:
                //first line in entry - Stewart, Jon
                //printf("%s\n", "should be first line");
                    //extract full nameline for printing
                    d->nameline = strdup(buf);
                   // printf("buf is: %s\n", buf);
                    //printf("%s%s\n", "nameLine is: ", d->nameline);
                    //extract last name for compare
                    d->surname = createAndcopyUntil(buf, ','); 
                    if (d->surname != NULL){
                    //printf("surname is: %s\n", d->surname);
                    }
                    break;
                case 2:
                //2nd line in entry - 1234 Madison Avenue OR University of Oregon
                 //printf("%s\n", "should be second line");
                    //extract full addressline for printing
                    d->addressline = strdup(buf);
                    //printf("%s%s\n", "addressLine is: ", d->addressline);
                    //check if first char is a letter
                    if (isalpha(buf[0])){
                        //it is, store housenumber as '0'
                        //need to actually malloc this
                        d->housenumber = (char *)malloc(sizeof(char)*2);
                        strcpy(d->housenumber, "0");
                    }
                    //else it is a number, store it
                    else{
                        //this may not be working right
                        d->housenumber = createAndcopyUntil(buf, ' ');
                    }
                    if (d->housenumber != NULL){
                    //printf("%s%s\n", "housenumber is: ", d->housenumber);
                }
                    break;
                case 0:
                //printf("%s\n", "should be third line");
                //3rd line in entry - Durham, NC 27708
                    //extract full line for printing
                    d->locationline = strdup(buf);
                    //printf("%s%s\n", "locationLine is: ", d->locationline);
                    //extract zip
                    d->zipcode = getZip(buf);
                   // printf("%s%s\n\n", "zipcode is: ", d->zipcode);
                    break;
                    }
            }
            //what is this doing?
            *me = template;
            //connect data to MEntry
            me->self = d; 
       
        }
        //else d is NULL
        else {
            free(me);
            me = NULL;

        }
    }
    return me;
}

    







